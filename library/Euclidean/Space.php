<?php
/**
 * This keeps track of all objects on the board (and off the board, for that matter)
 * @category          Euclidean
 * @package           Space
 * @copyright         Copyright (c) 2012 Barney Hanlon (http://www.shrikeh.net)
 * @author            B Hanlon <barney@shrikeh.net>
 * @version           Revision $LastChangedRevision$ by $LastChangedBy$ on $LastChangedDate$
 * @access            public
 */
namespace Euclidean;
/**
 * This keeps track of all objects on the board (and off the board, for that matter)
* @category          Euclidean
* @package           Space
* @copyright         Copyright (c) 2012 Barney Hanlon (http://www.shrikeh.net)
* @author            B Hanlon <barney@shrikeh.net>
* @version           Revision $LastChangedRevision$ by $LastChangedBy$ on $LastChangedDate$
* @access            public
*/
class Space
{
    protected $_board;

    protected $_objects;

    public function __construct(Board $board)
    {
        $this->_objects = new \SplObjectStorage();
    }

    /**
     * Get the board
     * @return Board
     */
    public function getBoard()
    {
        return $this->_board;
    }

    /**
     * Add a game object
     */
    public function addObject(GameObject $gameObject)
    {
        $board = $this->getBoard();
        // a game object must be within the area of a board to be part of
        // Euclidean space
        $this->_objects->attach($gameObject);
    }

    /**
     * Return all game objects
     * @return \SplObjectStorage
     */
    public function getObjects()
    {
        return $this->_objects;
    }
}
