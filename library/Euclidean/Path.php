<?php
namespace Euclidean;
use \Euclidean\Vector;
use \Euclidean\Coordinate\CoordinateInterface;

class Path
{
    protected $_coordinates = array();

    public function addCoordinate(CoordinateInterface $coordinate)
    {
        $this->_coordinates[] = $coordinate;
        return $this;
    }

    public function getCoordinates()
    {
        return $this->_coordinates;
    }

    public function by($heading)
    {
        foreach ($this->_coordinates as $coordinate) {
            $coordinate->setZenith($heading);
        }
        return $this->_coordinates;
    }


    public function setCentre(CoordinateInterface $centre)
    {
        foreach ($this->getCoordinates() as $coordinate) {
            $coordinate->setOrigin($centre);
        }
        return $this;
    }
}
