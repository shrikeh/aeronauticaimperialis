<?php
namespace Euclidean;
use \Euclidean\Shape\ShapeInterface;
class BoundingVolume
{
    protected $_shapes;

    protected $_boundingBox;

    public function __construct()
    {
        $this->_shapes = new \SplObjectStorage();
    }

    public function addShape(ShapeInterface $shape)
    {
        $this->_shapes->attach($shape);
        $this->clearBoundingBox();
    }


    public function getShapes()
    {
        return $this->_shapes;
    }

    public function getBoundingBox()
    {
        if (!$this->_boundingBox) {
            $minX = $this->_centre->getX();
            $minY = $this->_centre->getY();
            $minZ = $this->_centre->getZ();

            $maxX = $this->_centre->getX();
            $maxY = $this->_centre->getY();
            $maxZ = $this->_centre->getZ();

            foreach($this->_path->getCoordinates() as $coordinate) {
                $x = $coordinate->getX();
                $y = $coordinate->getY();
                $z = $coordinate->getZ();

                if ($x < $minX) {
                    $minX = $x;
                }
                if ($y < $minY) {
                    $minY = $y;
                }
                if ($z < $minZ) {
                    $minZ = $z;
                }
                if ($x > $maxX) {
                    $maxX = $x;
                }
                if ($y > $maxY) {
                    $maxY = $y;
                }
                if ($z > $maxZ) {
                    $maxZ = $z;
                }
            }
            $this->_boundingBox = new BoundingBox(
                $minX,
                $minY,
                $minZ,
                $maxX,
                $maxY,
                $maxZ
            );
        }
        return $this->_boundingBox;
    }

    /**
     * A bounding box simplifies most of our calculations. If there is no intersection
     * with the shape's bounding box, then there is no need to do anything further.
     */
    public function intersects(CoordinateInterface $coordinate)
    {


    }
}
