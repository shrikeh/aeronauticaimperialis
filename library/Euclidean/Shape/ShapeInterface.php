<?php
namespace Euclidean\Shape;
use \Euclidean\Coordinate\CoordinateInterface;

interface ShapeInterface
{
    public function getArea();

    public function within(CoordinateInterface $coordinate);

    public function getCentre();

    public function setCentre(CoordinateInterface $coordinate);

    public function getBoundingBox();

}
