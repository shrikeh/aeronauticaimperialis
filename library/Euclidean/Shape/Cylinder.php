<?php
namespace Euclidean\Shape;
use \Euclidean\Coordinate\CoordinateInterface;

class Cylinder extends ShapeAbstract
{
    protected $_radius;

    protected $_length;

    protected $_height;

    public function __construct($radius, $length, $height = 0)
    {
        $this->_radius = $radius;
        $this->_length = $length;
        $this->_height = $height;
    }

    public function getArea()
    {

    }

    public function getVolume()
    {
        return pi() * ($this->_radius * $this->_radius) * $this->_height;
    }

    public function within()
    {

    }


}
