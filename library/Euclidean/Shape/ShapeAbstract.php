<?php
namespace Euclidean\Shape;
use \Euclidean\Coordinate\CoordinateInterface;

abstract class ShapeAbstract implements ShapeInterface
{
    protected $_centre;

    protected $_boundingBox;

    public function getCentre()
    {
        return $this->_centre;
    }

    public function setCentre(CoordinateInterface $coordinate)
    {
        $this->_centre = $coordinate;
    }

}
