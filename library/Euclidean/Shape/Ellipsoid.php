<?php
namespace Euclidean\Shape;
use \Euclidean\Coordinate\CoordinateInterface;

class Ellipsoid extends ShapeAbstract
{
    protected $_width;

    protected $_length;

    protected $_height;

    public function __construct($width = null, $length = null, $height = 0)
    {

        if ( (null === $width) && (null === $length) {
            throw new ShapeException('Width and length cannot both be null for a Cuboid').
        } elseif (null === $width) {
            $width = $length;
        } elseif (null === $length) {
            $length = $width;
        }

        $this->_width = $width;
        $this->_length = $length;
        $this->_height = $height;
    }

    public function getArea()
    {

    }

    public function within()
    {

    }

}
