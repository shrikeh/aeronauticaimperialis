<?php
namespace Euclidean\Shape;
use \Euclidean\Coordinate\CoordinateInterface;

class Cuboid extends ShapeAbstract
{
    protected $_width;

    protected $_length;

    protected $_height;

    public function __construct($width, $length, $height = 0)
    {
        $width = intval($width);
        $length = intval($length);
        $height = intval($height);
        if ( (0 === $width) && (0 === $length) ) {
            throw new ShapeException('Width and length cannot both be 0 for a Cuboid');
        } elseif (0 === $width) {
            $width = $length;
        } elseif (0 === $length) {
            $length = $width;
        }

        $this->_width = $width;
        $this->_length = $length;
        $this->_height = $height;
    }


    public function getArea()
    {

    }

    public function within(CoordinateInterface $coordinate)
    {

    }

    public function getBoundingBox()
    {

    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function getLength()
    {
        return $this->_length;
    }

}
