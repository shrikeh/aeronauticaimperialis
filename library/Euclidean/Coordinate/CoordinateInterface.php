<?php
namespace Euclidean\Coordinate;

interface CoordinateInterface
{

    const DEGREE_MAX = 360;

    public function getDistance(CoordinateInterface $coordinate);

    public function getX();

    public function getY();

    public function getZ();

    public function render();

    public function angleFrom(CoordinateInterface $coordinate);

    public function angleTo(CoordinateInterface $coordinate);
}
