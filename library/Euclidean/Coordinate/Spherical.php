<?php
use Euclidean\Coordinate\CoordinateInterface;

namespace Euclidean\Coordinate;

class Spherical extends CoordinateAbstract
{

    protected $_origin;

    protected $_radius;

    protected $_azimuth;

    protected $_zenith;

    protected $_inclination;

    public function __construct($radius, $azimuth, $inclination, $zenith = 0)
    {
        $this->_radius = $radius;
        $this->_azimuth = $azimuth;
        $this->_zenith = $zenith;
        $this->_inclination = $inclination;
    }

    public function getAzimuth()
    {
        return $this->_azimuth;
    }

    public function setOrigin(CoordinateInterface $origin = null)
    {
        $this->_origin = $origin;
    }

    protected function _getOrigin()
    {
        if (!$this->_origin) {
            throw new CoordinateException('Origin has not been set!');
        }
        return $this->_origin;
    }

    public function setZenith($zenith)
    {
        $this->_zenith = $zenith;
    }

    public function getX()
    {
        $angle = $this->_normalizeAngle($this->_azimuth + $this->_zenith);
        return $this->_getOrigin()->getX() + $this->_radius * sin(deg2rad($angle));
    }

    public function getY()
    {
        $angle = $this->_normalizeAngle($this->_azimuth + $this->_zenith);
        return $this->_getOrigin()->getY() + $this->_radius * cos(deg2rad($angle));
    }

    public function getZ()
    {

    }
}
