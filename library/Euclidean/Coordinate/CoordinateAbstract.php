<?php
namespace Euclidean\Coordinate;

abstract class CoordinateAbstract implements CoordinateInterface
{

    protected $_origin;

    public function getDistance(CoordinateInterface $coordinate)
    {
        $xOffset = abs($coordinate->getX() - $this->getX());
        $yOffset = abs($coordinate->getY() - $this->getY());

        $hypot = hypot($xOffset, $yOffset);
        return $hypot;
    }

    public function __toString()
    {
        return $this->render();
    }

    public function render()
    {
        $mask = '(%d,%d,%d)';
        return sprintf(
            $mask,
            $this->getX(),
            $this->getY(),
            $this->getZ()
        );
    }

    public function getOrigin()
    {
        return $this->_origin;
    }

    public function setOrigin(CoordinateInterface $origin)
    {
        $this->_origin = $origin;
    }

    public function angleFrom(CoordinateInterface $coordinate)
    {
        return $this->_angle($this, $coordinate);
    }

    public function angleTo(CoordinateInterface $coordinate)
    {
        return $this->_angle($coordinate, $this);
    }

    protected function _angle(CoordinateInterface $one, CoordinateInterface $two)
    {
        $xOffset = $one->getX() - $two->getX();
        $yOffset = $one->getY() - $two->getY();
        $d = rad2deg(atan2($yOffset, $xOffset));
        return $this->_normalizeAngle($d);
    }

    protected function _normalizeAngle($angle)
    {
        $d = $angle % self::DEGREE_MAX;
        return $d < 0 ? $d + self::DEGREE_MAX : $d;
    }
}
