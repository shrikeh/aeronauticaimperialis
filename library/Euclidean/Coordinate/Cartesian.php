<?php
namespace Euclidean\Coordinate;

class Cartesian extends CoordinateAbstract
{
    /**
    * Cartesian Vector
    * @var integer
    **/
    protected $_x;

    /**
    * Cartesian Vector
    * @var integer
    **/
    protected $_y;

    /**
    * The height in altitiude.
    * @var integer
    **/
    protected $_z;

    public function __construct($xAxis = 0, $yAxis = 0, $zAxis = 0)
    {
        $this->setX($xAxis);
        $this->setY($yAxis);
        $this->setZ($zAxis);
    }


    public function __set($axis, $value)
    {
        $method = $method = 'set' . strtoupper($axis);
        return $this->$method($value);
    }

    public function __get($axis)
    {
        $method = 'get' . strtoupper($axis);
        return $this->$method();
    }


    public function getX()
    {
        $x = $this->_x;
        if ($this->_origin) {
            $x = $x + $this->_origin->getX();
        }
        return $x;
    }

    public function setX($x)
    {
        $this->_x = $x;
        return $this;
    }

    public function getY()
    {
        $y = $this->_y;
        if ($this->_origin) {
            $y = $y + $this->_origin->getY();
        }
        return $y;
    }

    public function setY($y)
    {
        $this->_y = $y;
        return $this;
    }

    public function getZ()
    {
        $z = $this->_z;
        if ($this->_origin) {
            $z = $z + $this->_origin->getZ();
        }
        return $z;
    }

    public function setZ($z)
    {
        $this->_z = (int) $z;
        return $this;
    }

    public function move($distance, $angle)
    {
        $heading = deg2rad($angle);
        $this->setX($this->_x + $distance * sin($heading));
        $this->setY($this->_y + $distance * cos($heading));

        return $this;
    }
}
