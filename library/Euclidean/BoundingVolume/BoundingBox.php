<?php
namespace Euclidean\BoundingVolume;
use \Euclidean\Coordinate\CoordinateInterface;

class BoundingBox
{
    protected $_minX;

    protected $_maxX;

    protected $_minY;

    protected $_maxY;

    protected $_minZ;

    protected $_maxZ;

    public function __construct($minX, $minY, $minZ, $maxX, $maxY, $maxZ)
    {
        $this->_minX = $minX;
        $this->_maxX = $maxX;
        $this->_minY = $minY;
        $this->_maxY = $maxY;
        $this->_minZ = $minZ;
        $this->_maxZ = $maxZ;
    }

    public function getMinX()
    {
        return $this->_minX;
    }

    public function getMaxX()
    {
        return $this->_maxX;
    }

    public function getMinY()
    {
        return $this->_minY;
    }

    public function getMaxY()
    {
        return $this->_maxY;
    }

    public function getMinZ()
    {
        return $this->_minZ;
    }

    public function getMaxZ()
    {
        return $this->_maxZ;
    }

    public function within(CoordinateInterface $coordinate)
    {
        if (
                ($coordinate->getX() <= $this->getMaxX()) &&
                ($coordinate->getY() <= $this->getMaxY()) &&
                ($coordinate->getZ() <= $this->getMaxZ()) &&
                ($coordinate->getX() >= $this->getMinX()) &&
                ($coordinate->getY() >= $this->getMinY()) &&
                ($coordinate->getZ() >= $this->getMinZ())
        ) {
            return true;
        }
        return false;
    }
}
