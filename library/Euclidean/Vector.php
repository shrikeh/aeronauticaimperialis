<?php
namespace Euclidean;
/**
 * Used to describe the centre of a GameObject. All math is done against this object,
 * and the co-ordinates of the outline are adjusted accordingly. Co-ordinates therefore are
 * representations from this point outwards.
 * @author barneyhanlon
 *
 */
class Vector
{

    protected $_coordinate;

    /**
    * @var integer
    **/
    protected $_heading;

    /**
    * The current speed
    **/
    protected $_speed;

    public function __construct(Coordinate\CoordinateInterface $coordinate, $heading = 0, $speed = 0)
    {
        $this->_coordinate = $coordinate;
        $this->setHeading($heading);
        $this->setSpeed($speed);
    }

    public function getCoordinate()
    {
        return $this->_coordinate;
    }

    public function getSpeed()
    {
        return $this->_speed;
    }

    public function setSpeed($speed)
    {
        $this->_speed = $speed;
        return $this;
    }

    public function setHeading($heading)
    {
        $this->_heading = (int) $heading;
        return $this;
    }

    public function getHeading()
    {
        return $this->_heading;
    }

    public function move()
    {
        $this->getCoordinate()->move($this->_speed, $this->_heading);
        return $this;
    }
}
