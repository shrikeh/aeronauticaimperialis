<?php
namespace AeronauticaImperialis;
use  Wargame\Faction\FactionAbstract;
class Race extends FactionAbstract
{

    protected $_baseSkill;

    public function __construct($name, $skill)
    {
        $this->_name = $name;
        $this->_baseSkill = $skill;
    }

    public function getBaseSkill()
    {
        return $this->_baseSkill;
    }
}
