<?php
namespace AeronauticaImperialis;
use \Wargame\GameSystem\GameSystemAbstract;
class GameSystem extends GameSystemAbstract
{
    const PHASE_MANOUEVRE    = 0;
    const PHASE_INITIATIVE   = 1;
    const PHASE_TAILING      = 2;
    const PHASE_MOVEMENT     = 3;
    const PHASE_SHOOTING     = 4;
    const PHASE_END          = 5;

    const MAX_TURNS = 12;

    CONST RANGE_DISTANCE_SHORT = 6;

    CONST RANGE_DISTANCE_MEDIUM = 12;

    CONST RANGE_DISTANCE_LONG = 18;

    protected $_name = 'Aeronautica Imperialis';

    protected $_boardHeightLevels = 10;


}
