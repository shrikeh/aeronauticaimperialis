<?php
namespace AeronauticaImperialis;
class Manoeuvre
{

    const LOW = 'low';
    CONST HIGH = 'high';
    const ULTRA_HIGH = 'ultra-high';

    /**
    * The ID of the manouvre from 1 - 10
    * Enter description here ...
    * @var unknown_type
    */
    protected $_id;

    protected $_name;

    protected $_rating;

    protected $_vector;

    public function __construct(Vector $vector, $id, $name, $rating)
    {
        $this->_id = $id;
        $this->_name = $name;
        $this->_rating = $rating;
        $this->_vector = $vector;
    }

    public function manouevre(Manoeuvre $manoeuvre)
    {
        $this->setHeading($this->_heading + $manoeuvre->getHeading());
        $this->setSpeed($this->_speed + $manoeuvre->getSpeed());
        $this->setAltitude($this->_altitude($manoeuvre->getAltitude()));
        $this->setX($manoeuvre->getX());
        $this->setY($manoeuvre->getY());
        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getRating()
    {
        return $this->_rating;
    }

    public function perform(Vector $vector)
    {
        $newHeading = $vector->getHeading() + $this->_vector->getHeading();
        $newSpeed = $vector->getSpeed() + $this->_vector->getSpeed();
        $newX = $vector->getX() + $this->_vector->getX();
        $newY = $vector->getY() + $this->_vector->getY();
        $newZ = $vector->getZ() + $this->_vector->getZ();

        $vector->setHeading($newHeading);
        $vector->setSpeed($newSpeed);
        $vector->setX($newX);
        $vector->setY($newY);
        $vector->setZ($newZ);

        return $vector;
    }

}
