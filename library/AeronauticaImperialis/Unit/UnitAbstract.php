<?php
namespace AeronauticaImperialis\Unit;

abstract class UnitAbstract implements UnitInterface
{
   /**
    * The name of the type of object, i.e. "Thunderbolt"
    */
    protected $_name;
    /**
     *
     * An array of weapons mounted on the unit
     * @var array
     */
    protected $_weapons = array();

    /**
     * The type of the unit, e.g. ground target, aircraft, etc.
     * Enter description here ...
     * @var string
     */
    protected $_type;

    /**
     * The number of hits the unit starts with
     * @var integer
     */
    protected $_hits;

    public function setWeapons($weapons = array())
    {
        $this->_weapons = new ArrayObject($weapons);
        return $this;
    }


}
