<?php
namespace AeronauticaImperialis\Unit;

class AircraftException extends \AeronauticaImperialis\Exception
{
    const MANOEUVRE_TOO_DIFFICULT = 1;
    const STALL_SPEED = 2;
    const TOO_FAST = 4;
    const TOO_HIGH = 8;
    const HIT_THE_DECK = 16;
}
