<?php
namespace AeronauticaImperialis\Unit;

class Aircraft extends UnitAbstract
{
    const TYPE_FIGHTER = 'fighter';

    const TYPE_BOMBER = 'bomber';

    protected $_manoeuvrability;

    protected $_speed;

    protected $_thrust;

    protected $_minSpeed;

    protected $_maxSpeed;

    protected $_maxAltitude;

    public function __construct(
        $name,
        Pilot $pilot,
        $hits,
        $manoeuvrability,
        $maxAltitude,
        $thrust,
        $maxSpeed,
        $minSpeed = 0,
        $weapons = array()
    ) {
        $this->_setManoeuvrability($manoeuvrability);
    }

    /**
     * Return the maximum altitude of the aircraft
     * @return integer
     */
    public function getMaxAltitude()
    {
        return $this->_maxAltitude;
    }

   /**
    * Return the maximum speed of the aircraft
    * @return integer
    */
    public function getMaxSpeed()
    {
        return $this->_maxSpeed;
    }

    /**
    * Return the minimum speed of the aircraft
    * @return integer
    */
    public function getMinSpeed()
    {
        return $this->_minSpeed;
    }

    protected function _setManoeuvrability($manoeuvrability)
    {
        $this->_manoeuvrability = $manoeuvrability;
    }

    /**
     *
     * Enter description here ...
     * @param integer $thrust
     * @throws OutOfBoundsException
     */
    public function applyThrust($thrust)
    {
        if (abs($thrust) > $this->_unit->_thrust) {
            throw new \OutOfBoundsException('Thrust cannot be greater than ' . $this->_thrust);
        }
        $this->adjustSpeed($thrust);
        return $this;
    }

    public function manoeuvre(Manoeuvre $manoeuvre)
    {
        if ($manoeuvre->rating() > $this->getManoeuvrability()) {
            throw new AircraftException('This manoeuvre is too high for this piece!');
        }

        $this->_vector->manoeuvre($manoeuvre);

        if ($this->_vector->getSpeed() > $this->_unit->getMaxSpeed()) {
            throw new AircraftException('Going too fast!', AircraftException::TOO_FAST);
        }
        if ($this->_vector->getSpeed() < $this->_unit->getMinSpeed()) {
            throw new AircraftException('Stalling!', AircraftException::STALL_SPEED);
        }

        if ($this->_vector->getAltitude() > $this->_unit->getMaxAltitude()) {
            throw new AircraftException('Too high!', AircraftException::TOO_HIGH);
        }

        if ($this->_vector->getAltitude() < 0) {
            throw new AircraftException('Eating dirt!', AircraftException::HIT_THE_DECK);
        }
        return $this;
    }
}
