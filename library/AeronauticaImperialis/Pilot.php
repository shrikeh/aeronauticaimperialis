<?php
namespace AeronauticaImperialis;
use AeronauticaImperialis\Dice;
class Pilot
{

    const ACE = 5;

    const DOUBLE_ACE = 10;


    protected $_id;

    protected $_name;

    protected $_race;

    /**
     * Number of kills this pilot has
     * @var integer
     */
    protected $_kills;

    public function __construct(Race $race, $name, $kills = 0)
    {
        $this->_name = $name;
        $this->_setRace($race);
        $this->_kills = $kills;
    }

	public function getRace()
	{
		return $this->_race;
	}

	public function getKills()
	{
		return $this->_kills;
	}

    public function getSkill()
    {
        $skill = $this->_race->getBaseSkill();

        if ($this->isAce()) {
            $skill-= 1;
        }

        if ($this->isDoubleAce()) {
            $skill-= 1;
        }

        return $skill;
    }

    public function addKill()
    {
        $this->_kills++;
    }

    public function isAce()
    {
        return (self::ACE <= $this->_kills);
    }

    public function isDoubleAce()
    {
        return (self::DOUBLE_ACE <= $this->_kills);
    }

    public function skillTest(Dice $dice)
    {
        $pilotSkill = $this->getSkill();
        return ($dice->greaterThanOrEqualTo($pilotSkill));
    }

    protected function _setRace(Race $race)
    {
        $this->_race = $race;
    }
}
