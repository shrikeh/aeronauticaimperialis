<?php
namespace AeronauticaImperialis;
class Action
{
	protected $_unit;
	
	protected $_vector;
	
	public function setUnit(UnitInterface $unit)
	{
		$this->_unit = $unit;
	}
	
	public function setVector(Vector $vector)
	{
		$this->_vector = $vector;
	}
}