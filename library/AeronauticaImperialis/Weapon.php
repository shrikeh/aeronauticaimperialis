<?php
namespace AeronauticaImperialis;
use \SplFixedArray;
use AeronauticaImperialis\Weapon\OutOfAmmoException;
use AeronauticaImperialis\Weapon\WeaponException;
class Weapon
{

    CONST SUSTAINED_FIRE_MULTIPLIER = 1.5;

    /**
    * Constant used to describe weapons with unlimited ammo.
    * @var integer
    */
    const UNLIMITED_AMMO = -1;

    const SHORT_RANGE = 'short';

    const MEDIUM_RANGE = 'medium';

    const LONG_RANGE = 'long';

    /**
    * The name of the weapon, i.e. "lascannon"
    * @var string
    */
    protected $_name;

    /**
     * number of dice rolled at different ranges
     * @var SplFixedArray
     */
    protected $_ranges;

    /**
     * The amount of damage this weapon does normally.
     * Enter description here ...
     * @var integer
     */
    protected $_damageAmount = 1;

    /**
     * The roll needed to damage, normally from 2 - 6
     * @var integer
     */
    protected $_damage;


    /**
     * The score needed to cause additional damage.
     * If this is 0, it does no additional damage.
     * Enter description here ...
     * @var integer
     */
    protected $_extraDamage = 0;

   /**
    * How much ammo the weapon currently has
    * @var integer
    */
    protected $_currentAmmo;

    /**
    * The bitwise value for the arcs of this weapon
    * @var integer
    */
    protected $_arc;

    protected $_fired = false;

    protected static $_rangeNameMap;

    protected static $_rangeDistanceMap;

    public static function getRangeDistanceMap()
    {
        self::_setMaps();
        return self::$_rangeDistanceMap;
    }

    public static function getRangeNameMap()
    {
        self::_setMaps();
        return self::$_rangeNameMap;
    }

    /**
     * Constructor
     */
    public function __construct(
        $damage,
        $ranges,
        $ammo,
        $extraDamage = null,
        $special = null
    )
    {
        self::_setMaps();
        $this->_damage = $damage;
        $this->_setRanges($ranges);
        $this->_currentAmmo = $ammo;
        $this->_extraDamage = $extraDamage;
    }

    public function getDamage()
    {
        return $this->_damage;
    }

    public function getExtraDamage()
    {
        return $this->_extraDamage;
    }


    /**
     * @param Dice $dice
     * @return integer Amount of damage caused
     */
    public function doesDamage(Dice $dice)
    {
        $damage = 0;
        if ($dice->greaterThanOrEqualTo($this->_damage)) {
            $damage+= $this->_damageAmount;
        }
        // If the weapon does extra damage...
        if (($this->_extraDamage) && ($dice->greaterThanOrEqualTo($this->_extraDamage)) ) {
            $damage+= $this->_damageAmount;
        }
        return $damage;
    }

    public function resetHasFired()
    {
        $this->_fired = false;
    }

    public function expendAmmo($ammoExpenditure = 1)
    {
        if (self::UNLIMITED_AMMO == $this->_currentAmmo) {
            return;
        }
        $this->_currentAmmo = $this->_currentAmmo - $ammoExpenditure;
        return $this->_currentAmmo;
    }

    /**
    * Fire the weapon and reduce the ammo.
    * @param integer $range
    * @param boolean $sustainedBurst
    * @throws OutOfBoundsException
    * @return The number of dice to roll to hit
    */
    public function fire($range, $sustainedBurst = false)
    {
        $diceToRoll = 0;
        if ($this->_fired) {
            throw new WeaponException('Weapon has already fired this turn');
        }
        if ( ($sustainedBurst) && (self::UNLIMITED_AMMO == $this->_currentAmmo) ) {
            throw new WeaponException('Unlimited ammo weapons cannot sustained burst');
        }

        $ammoExpenditure = ($sustainedBurst) ? 2 : 1;

        if (!$this->hasAmmo($ammoExpenditure)) {
            throw new OutOfAmmoException('Insufficient ammo to fire');
        } else {
            $this->expendAmmo($ammoExpenditure);
            $this->_fired = true;
        }

        // now we actually fire.
        $diceToRoll = $this->getDiceAtRange($range);

        if ($sustainedBurst) {
            $diceToRoll = $diceToRoll * self::SUSTAINED_FIRE_MULTIPLIER;
        }
        return ceil($diceToRoll);
    }

    public function getCurrentAmmo()
    {
        return $this->_currentAmmo;
    }

    /**
     *
     * Enter description here ...
     * @param integer $range
     */
    public function getDiceAtRange($range)
    {
        $normalizedRange = $this->_normalizeRange($range);
        if (null === $normalizedRange) {
            // out of range anyway
            return null;
        } else {
            return $this->_ranges->offsetGet($normalizedRange);
        }
    }

    /**
    * To keep a list of ranges and their short titles, we set up
    * two SplFixedArrays for use. We make these static to be available
    * across all instances and conserve memory.
    **/
    protected static function _setMaps()
    {
        if (!self::$_rangeNameMap) {
            self::$_rangeNameMap = new SplFixedArray(3);
            self::$_rangeNameMap->offsetSet(0, self::SHORT_RANGE);
            self::$_rangeNameMap->offsetSet(1, self::MEDIUM_RANGE);
            self::$_rangeNameMap->offsetSet(2, self::LONG_RANGE);
        }

        if (!self::$_rangeDistanceMap) {
            self::$_rangeDistanceMap = new SplFixedArray(3);
            self::$_rangeDistanceMap->offsetSet(0, self::RANGE_DISTANCE_SHORT);
            self::$_rangeDistanceMap->offsetSet(1, self::RANGE_DISTANCE_MEDIUM);
            self::$_rangeDistanceMap->offsetSet(2, self::RANGE_DISTANCE_LONG);
        }
    }

    /**
     * Set up the array of ranges for this weapon.
     * @param array|Iterator $ranges
     */
    protected function _setRanges($ranges)
    {
        ksort($ranges);
        $this->_ranges = new SplFixedArray(3);

        foreach (self::$_rangeNameMap as $position => $key) {
            $value = $ranges[$position];
            $this->_ranges->offsetSet($position, $value);
        }
        return $this->_ranges;
    }

    public function hasAmmo($ammoExpenditure = 1)
    {
        if (self::UNLIMITED_AMMO == $this->_currentAmmo) {
            return true;
        }
        return (($this->_currentAmmo - $ammoExpenditure) >= 0);
    }

    /**
     * Normalize the range of a weapon into one of the three brackets.
     * @param integer|float $range
     */
    protected function _normalizeRange($range)
    {
        assert('$range >= 0');
        foreach (self::$_rangeDistanceMap as $key => $distance) {
            if (ceil($range) <= $distance) {
                return $key;
            }
        }
        return null;
    }
}
