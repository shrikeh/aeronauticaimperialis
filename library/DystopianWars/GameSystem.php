<?php
namespace DystopianWars;
use \Wargame\GameSystem\GameSystemAbstract;
class GameSystem extends GameSystemAbstract
{
    const HEIGHT_SUBMERGED = 0;
    const HEIGHT_SURFACE = 1;
    const HEIGHT_FLYING = 2;
    const HEIGHT_OBSCURED = 3;


    protected $_name = 'Dystopian Wars';

    protected $_boardHeightLevels = 4;
}
