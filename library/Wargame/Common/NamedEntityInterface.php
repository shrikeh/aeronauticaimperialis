<?php
namespace Wargame\Common;

interface NamedEntityInterface
{
    public function getName();
}
