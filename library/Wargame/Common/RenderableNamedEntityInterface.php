<?php
namespace Wargame\Common;

interface RenderableNamedEntityInterface extends RenderableInterface, NamedEntityInterface
{

}
