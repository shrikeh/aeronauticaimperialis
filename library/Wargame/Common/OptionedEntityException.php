<?php
/**
 * An Optioned entity means that it accepts a config file as one of its constructors,
 * or after instantiation, and acts as a repository for these options. This is the preferred
 * exception should something go wrong within one of the defined functions in the interface
 * \Wargame\Common\OptionedEntityInterface.
 *
 * @category          Wargame
 * @package           Common
 * @copyright         Copyright (c) 2012 Barney Hanlon (http://www.shrikeh.net)
 * @author            B Hanlon <barney@shrikeh.net>
 * @version           Revision $LastChangedRevision$ by $LastChangedBy$ on $LastChangedDate$
 * @access            public
 * @see               \Wargame\Common\OptionedEntityInterface
 * @internal
 */
namespace Wargame\Common;
use \Wargame\WargameException;
class OptionedEntityException extends WargameException
{

}
