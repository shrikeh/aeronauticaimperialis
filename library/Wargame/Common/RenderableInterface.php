<?php
namespace Wargame\Common;
interface RenderableInterface
{
    public function __toString();

    public function render();
}
