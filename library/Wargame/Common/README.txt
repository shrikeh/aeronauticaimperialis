This includes the common interfaces shared by the Wargame library.
As PHP is not a multi-inheritance language (traits aside), the need to use
interfaces heavily allows (rather then hinders) understanding of use case.
