<?php
/**
 * An Optioned entity means that it accepts a config file as one of its constructors,
 * or after instantiation, and acts as a repository for these options.
 * @category          Wargame
 * @package           Common
 * @copyright         Copyright (c) 2012 Barney Hanlon (http://www.shrikeh.net)
 * @author            B Hanlon <barney@shrikeh.net>
 * @version           Revision $LastChangedRevision$ by $LastChangedBy$ on $LastChangedDate$
 * @access            public
 */

namespace Wargame\Common;
/**
 * An Optioned entity means that it accepts a config file as one of its constructors,
 * or after instantiation, and acts as a repository for these options.
 * @category          Wargame
 * @package           Common
 * @copyright         Copyright (c) 2012 Barney Hanlon (http://www.shrikeh.net)
 * @author            B Hanlon <barney@shrikeh.net>
 * @version           Revision $LastChangedRevision$ by $LastChangedBy$ on $LastChangedDate$
 * @access            public
 */
interface OptionedEntityInterface
{
   /**
    * Set the options for this object
    * @param array|Iterator|Zend_Config $config
    */
    public function setOptions($config = array());

    /**
     * Set a single option for this object
     * @param string The key name for the option
     * @param mixed Value for this option
     */
    public function setOption($key, $value);
    /**
    * If the option has been set
    * @param string $key
    * @return boolean
    */
    public function hasOption($key);

    /**
     * Return an option by key name
     * @param string $key
     * @return mixed
     * @throws OptionedEntityException if the option hasn't been set
     */
    public function getOption($key);

    /**
     * Return all options
     * @return array|\Iterator
     */
    public function getOptions();
}
