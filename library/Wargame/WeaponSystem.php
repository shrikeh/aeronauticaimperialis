<?php
namespace Wargame;
use \Wargame\Geometry\Arc;
use \Wargame\Common\RenderableNamedEntityInterface;
class WeaponSystem implements RenderableNamedEntityInterface
{
    protected $_name;

    protected $_arc;

    protected $_weapon;

    public function __construct(WeaponInterface $weapon, Arc $arc, $name)
    {
        $this->_weapon = $weapon;
        $this->_name = $name;
        $this->_arc = $arc;
    }

    public function __toString()
    {
        return $this->render();
    }

    public function render()
    {
        return (string) $this->getName();
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getWeapon()
    {
        return $this->_weapon;
    }

    public function inArc($angle)
    {
        return $this->_arc->in($angle);
    }

}
