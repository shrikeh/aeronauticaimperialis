<?php
namespace Wargame;
use \SplFixedArray;
class Turn
{
    protected $_id;

    protected $_phases;

    public function __construct($id, $phases)
    {
        $this->_id = $id;
    }

    protected function _setUpPhases()
    {
        $this->_phases = new SplFixedArray(6);
    }
}
