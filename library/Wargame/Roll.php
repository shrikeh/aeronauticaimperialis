<?php
namespace Wargame;
use Wargame\Tools\Dice;
use \SplFixedArray;
class Roll
{
    protected $_dice;

    public function __construct($diceCount = 1, $sides = 6, GeneratorInterface $generator = null)
    {
        $this->_dice = new SplFixedArray($diceCount);
        for ($i=0; $i<$diceCount; $i++) {
            $this->_dice[$i] = new Dice($sides, $generator)
        }
    }

    public function setValues(array $values = array())
    {
        foreach ($values as $key => $value) {
            $this->_dice[$key] = $value;
        }
    }

    public function shake()
    {
        foreach ($this->_dice as $die) {
            $die->roll();
        }
    }

    public function getDice()
    {
        return $this->_dice;
    }

    public function getGreaterThanOrEqualTo($value)
    {
        $return = array();

        foreach ($this->_dice as $die) {
            if ($die->equalToOrGreaterThan($value)) {
                $return[] = $die;
            }
        }
        return $return;
    }

    public function reRoll()
    {
        foreach($this->_dice as $die) {
            $die->reroll();
        }
    }
}
