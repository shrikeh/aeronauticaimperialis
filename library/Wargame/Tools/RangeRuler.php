<?php
namespace Wargame\Tools;
use \Wargame\Geometry\Measurement;
class RangeRuler
{

    const INCHES        = 'inches';
    const CENTIMETRES   = 'centimetres';
    const MICROMETRES   = 'micrometres';

    const INCHES_SUFFIX = '"';
    const CENTIMETRES_SUFFIX = 'cm';

    const IMPERIAL = 'imperial';

    const METRIC = 'metric';

    protected $_system;

    protected $_unit;

    protected $_suffix;

    public function __construct($system = self::IMPERIAL, $unit = self::INCHES, $suffix = null)
    {
        $this->_system = $system;
        $this->_unit = $unit;
        $this->_suffix = $suffix;
    }

    public function __invoke($value)
    {
        return $this->getMeasurement($value);
    }

    public function getUnit()
    {
        return $this->_unit;
    }

    public function getSystem()
    {
        return $this->_system;
    }

    public function getSuffix()
    {
        return $this->_suffix;
    }

    public function getMeasurement($value)
    {
        return $this->buildMeasurement($value, $this->_unit, $this->_suffix);
    }

    public function fromBase($value)
    {
        $measurement = $this->buildMeasurement($value, self::MICROMETRES, null);
        return $measurement->toUnit($this->_unit);
    }


    /**
     * The "base" value is the distance in micrometres, so this
     * function will take a value such as 12 inches and return a value of
     *
     * @param float $value
     */
    public function toBase($value)
    {
        return $this->getMeasurement($value)->toBase();
    }

    public function buildMeasurement($value, $unit, $suffix = null)
    {
        return new Measurement($value, $unit, $suffix);
    }
}
