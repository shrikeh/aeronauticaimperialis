<?php
namespace Wargame\Tools\ShapeBuilder;

interface ShapeBuilderInterface
{
    public function build($shapeOptions);
}
