<?php
namespace Wargame\Tools;
use \Wargame\Tools\Dice\DiceException;
use \Wargame\Tools\Dice\Generator\GeneratorInterface;
class Dice
{
    protected $_sides;

    protected $_value;

    protected $_reRolled = false;

    protected $_generator;

    public function __construct($sides = 6, $generator = null)
    {
        $this->_sides = $sides;

        if ($generator) {
            $this->setGenerator($generator);
        }
    }

    public function __toString()
    {
        return (string) $this->getValue();
    }

    public function getValue()
    {
        if (null === $this->_value) {
            $this->roll();
        }
        return (int) $this->_value;
    }

    /**
    * Some people prefer saying it this way so it is a passthru method
    * @return boolean
    **/
    public function equalToOrGreaterThan($value)
    {
        return $this->greaterThanOrEqualTo($value);
    }

    /**
    * @return boolean
    **/
    public function greaterThanOrEqualTo($value)
    {
        $value = $this->_validateValue($value);
        return ($this->getValue() >= $value);
    }

    public function greaterThan($value)
    {
        $value = $this->_validateValue($value);
        return ($this->getValue() > $value);
    }

    public function equals($value) {
        $value = $this->_validateValue($value);
        return ($value == $this->getValue());
    }

    public function equalToOrLessThan($value)
    {
        return $this->lessThanOrEqualTo($value);
    }
    public function lessThanOrEqualTo($value)
    {
        $value = $this->_validateValue($value);
        return ($this->getValue() <= $value) ;
    }

    public function lessThan($value)
    {
        $value = $this->_validateValue($value);
        return ($this->getValue() < $value);
    }

    public function roll()
    {
        if ($this->_value) {
            throw new DiceException('This die has already been rolled,  you must explicitly re-roll it');
        }
        return $this->setValue();
    }

    public function hasRerolled()
    {
        return $this->_reRolled;
    }


    public function reRoll($value = null)
    {
        if ($this->_reRolled) {
            throw new DiceException('A dice can only be re-rolled once!');
        }
        $this->_reRolled = true;
        $this->setValue($value);
    }

    public function setGenerator(GeneratorInterface $generator)
    {
        $this->_generator = $generator;
    }

    public function setValue($value = null)
    {
        if (null === $value) {
            $value = $this->_generateValue();
        }
        $value = $this->_validateValue($value);
        $this->_value = $value;
    }

    protected function _generateValue()
    {
        if(!$this->_generator) {
            throw new DiceException('Generator not set');
        }
        return $this->_generator->generate(1, $this->_sides);

    }

    protected function _validateValue($value)
    {
        assert('is_numeric($value)');

        if ( ($value < 1) || ($value > $this->_sides) ) {
            throw new \OutOfBoundsException("A die cannot be $value");
        }
        return (int) $value;
    }
}
