<?php
namespace Wargame\Tools\Dice\Generator;
interface GeneratorInterface
{
    public function generate($min, $max);
}
