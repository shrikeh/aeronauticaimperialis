<?php
namespace Wargame\Tools\Dice\Generator;

class MersenneTwister implements GeneratorInterface
{
    protected static $_seed;

    public function generate($min, $max)
    {
        if (null === self::$_seed) {
            $seed = (double) microtime() ^ posix_getpid();
            self::$_seed = crc32($seed);
            mt_srand(self::$_seed);
        }
        return mt_rand($min, $max);
    }
}
