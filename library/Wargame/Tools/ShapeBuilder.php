<?php
namespace Wargame\Tools;
use \Wargame\Tools\ShapeBuilder\ShapeBuilderInterface;

class ShapeBuilder implements ShapeBuilderInterface
{
    protected $_rangeRuler;

    public function __construct(RangeRuler $ruler)
    {
        $this->_rangeRuler = $ruler;
    }

    public function build($shapeOptions)
    {
        $shapeType = $shapeOptions['shape'];

        switch(strtolower($shapeType)) {
            case 'cuboid'    :
                $width = $this->_toMeasurement((isset($shapeOptions['width'])) ? $shapeOptions['width'] : null);
                $length = $this->_toMeasurement((isset($shapeOptions['length'])) ? $shapeOptions['length'] : null);
                $height = $this->_toMeasurement((isset($shapeOptions['height'])) ? $shapeOptions['height'] : null);
                $shape = new \Euclidean\Shape\Cuboid($width->toBase(), $length->toBase(), $height->toBase());
                break;
            case 'ellipsoid' :
                break;
            case 'polygon'    :
                break;
        }

        return $shape;
    }

    protected function _toMeasurement($measurement)
    {
        if (is_array($measurement)) {
            $unit = key($measurement);
            $value = current($measurement);
        } else {
            $unit = $this->_rangeRuler->getUnit();
            $value = $measurement;
        }
        return $this->_rangeRuler->buildMeasurement($value, $unit);
    }
}
