<?php
/**
 * Bitwise value for a front-facing weapon
 * @var integer
 */
CONST ARC_FRONT = 1;

/**
 * Bitwise value for a rear-facing weapon
 * @var integer
 */
CONST ARC_REAR = 2;

/**
 * Bitwise value for a left-facing weapon
 * @var integer
 */
CONST ARC_LEFT = 4;

/**
 * Bitwise value for a right-facing weapon
 * @var integer
 */
CONST ARC_RIGHT = 8;

/**
 * Having this as a constant saves calculation.
 * @var integer
 */
CONST ARC_ALLROUND = 15;

/**
 * Bitwise value for a weapon constrained to firing on the
 * same level or up.
 * @var integer
 */
CONST ARC_UP    = 16;

/**
 * Bitwise value for a weapon constrained to firing on the
 * same level or up.
 * @var integer
 */
CONST ARC_DOWN = 32;

public function __construct($quadrants = 4)
{

}
