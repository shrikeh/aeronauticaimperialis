<?php
namespace Wargame\Event;

interface EventListenerInterface extends \SplObserver
{
    public function getEventBroker();
}
