<?php

use Wargame\Event\EventListenerInterface;

namespace Wargame\Event\EventBroker;

abstract class BrokerAbstract implements BrokerInterface
{
    protected $_observers;

    protected $_event;

    public function __construct(\RecursiveIterator $iterator)
    {
        $this->_observers = $iterator;
    }

    public function getEvent()
    {

    }

    public function count()
    {

    }

    public function getFlags()
    {

    }

    public function accept()
    {

    }

    public function current()
    {

    }

    public function getInnerIterator()
    {

    }

    public function getChildren()
    {

    }

    public function hasChildren()
    {

    }

    public function hasNext()
    {

    }

    public function key()
    {

    }

    public function next()
    {

    }

    public function rewind()
    {

    }

    public function valid()
    {

    }

    public function offsetExists($index)
    {

    }

    public function offsetGet($index)
    {

    }

    public function offsetSet($index, $value)
    {

    }

    public function offsetUnset($offset)
    {

    }


    public function attach(\SplObserver $observer)
    {
        $this->_observers->attach($observer);
    }

    public function detach(\SplObserver $observer)
    {

    }

    public function notify()
    {

    }

    public function toArray()
    {

    }
}
