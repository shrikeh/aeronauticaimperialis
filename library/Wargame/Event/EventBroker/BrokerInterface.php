<?php

use Wargame\Event\EventListenerInterface;

namespace Wargame\Event\EventBroker;

interface BrokerInterface extends \SplSubject, \ArrayAccess, \OuterIterator
{
    public function getEvent();

    public function toArray();
}
