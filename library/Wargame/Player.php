<?php
namespace Wargame;
use \Wargame\Common\RenderableNamedEntityInterface;
use \Wargame\Unit\UnitInterface;
/**
 *
 * Enter description here ...
 * @author barneyhanlon
 *
 */
class Player implements RenderableNamedEntityInterface
{
    protected $_name;

    /**
     *
     * Enter description here ...
     * @var ArrayObject
     */
    protected $_units;

    protected $_victoryPoints;

    public function __construct($name, $units = array())
    {
        $this->_name = $name;
        $this->_units = new ArrayIterator;
        $this->setUnits($units);
    }

    public function setUnits($units = array())
    {

        foreach ($units as $key => $unit) {
            $this->addUnit($unit, $key);
        }
        return $this->_units;
    }

    public function addUnit(UnitInterface $unit, $position = null)
    {
        $this->_units->offsetSet($unit, $position);
        return $this->_units;
    }
}
