<?php
namespace Wargame\Weapon;
use \Wargame\Common\RenderableNamedEntityInterface;

interface WeaponInterface extends RenderableNamedEntityInterface
{
    public function fire();

}
