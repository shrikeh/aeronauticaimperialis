<?php
namespace Wargame;
use Wargame\GameSystem\GameSystemInterface;

use \SplFixedArray;
class Game implements Iterator
{

    protected $_gameSystem;

    protected $_players;
    /**
    *
    * @var \SplFixedArray
    **/
    protected $_turns;

    public function __construct(GameSystemInterface $gameSystem, array $players)
    {
        $this->_gameSystem = $gameSystem;
        $this->_turns = new \SplObjectStorage();
        $this->setPlayers($players);
    }

    public function setPlayers($players)
    {
        $this->_players = new SplFixedArray(count($players));
        sort($players);
        foreach ($players as $key => $players) {
            $this->addPlayer($player, $key);
        }
    }

    public function addPlayer(Player $player, $position)
    {
        $this->_players->offsetSet($position, $player);
    }

    public function getCurrentTurn()
    {

    }

    public function setTurn(Turn $turn)
    {

    }
}
