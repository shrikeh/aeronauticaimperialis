<?php
namespace Wargame;

class Board
{
    protected $_width;

    protected $_length;

    protected $_heightLevels = 1;

    public function __construct($length, $width, $heightLevels = 1)
    {
        $this->_length = $length;
        $this->_width = $width;
        $this->_heightLevels = $heightLevels;
    }

    public function getHeightLevels()
    {
        return $this->_heightLevels;
    }

    /**
    * Return the length of the table
    */
    public function getLength()
    {
        return $this->_length;
    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function getArea()
    {
        return $this->getLength() * $this->getWidth();
    }

}
