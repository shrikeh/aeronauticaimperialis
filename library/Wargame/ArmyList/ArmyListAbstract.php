<?php
namespace Wargame\ArmyList;
abstract class ArmyListAbstract implements ArmyListInterface
{
    public function __construct(\RecursiveIterator $iterator)
    {

    }

    public function __toString()
    {

    }

    public function count()
    {

    }

    public function getCache()
    {

    }

    public function getFlags()
    {

    }

    public function accept()
    {

    }

    public function current()
    {

    }

    public function getInnerIterator()
    {

    }

    public function getChildren()
    {

    }

    public function hasChildren()
    {

    }

    public function hasNext()
    {

    }

    public function key()
    {

    }

    public function next()
    {

    }

    public function rewind()
    {

    }

    public function valid()
    {

    }

    public function offsetExists($index)
    {

    }

    public function offsetGet($index)
    {

    }

    public function offsetSet($index, $value)
    {

    }
}
