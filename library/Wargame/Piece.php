<?php
namespace Wargame;
use Wargame\Unit\UnitInterface;

class Piece
{
    protected $_unit;

    protected $_actions;

    protected $_hitsTaken = 0;

    /**
     * The vector of the piece
     * @var Vector
     */
    protected $_vector;

    public function __construct(UnitInterface $unit, Vector $vector, $hitsTaken = 0)
    {
        $this->_unit = $unit;
        $this->_vector = $vector;
        $this->_hitsTaken = $hitsTaken;
    }

    public function isDestroyed()
    {
        return ($this->_hitsTaken >= $unit->getHits());
    }

    public function isDamaged()
    {
        return (bool) $this->_hitsTaken;
    }

    public function getActions()
    {
        return $this->_actions;
    }

    public function setActions($actions = array())
    {
        $this->_actions = new ArrayObject(array());
        foreach ($actions as $action) {
            $action->setUnit($this->_unit);
            $action->setVector($this->_vector);
            $this->addAction($action);
        }
    }

    public function addAction(Action $action)
    {
        $this->_actions->append($action);
    }


}
