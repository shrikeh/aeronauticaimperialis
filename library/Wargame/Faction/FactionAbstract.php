<?php
/**
 * A "faction" is basically one side in a battle. In some gaming systems, this is
 * a race, such as Orks or Elves. In others, such as Dystopian Wars, this might be
 * a nation. Whatever it might be, it has a few base properties, including a name
 * and an army list of allowed units and upgrades.
 * @author barneyhanlon
 *
 */
namespace Wargame\Faction;
use \Wargame\ArmyList\ArmyListInterface;

abstract class FactionAbstract implements FactionInterface
{
    protected $_name;

    protected $_armyList;


    public function getName()
    {
        return $this->_name;
    }

    public function getArmyList()
    {
        return $this->_armyList;
    }

    public function setArmyList(ArmyListInterface $armyList)
    {
        $this->_armyList = $armyList;
    }
}
