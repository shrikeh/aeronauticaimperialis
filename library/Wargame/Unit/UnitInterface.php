<?php
namespace Wargame\Unit;

interface UnitInterface extends \Iterator
{
    public function getName();
}
