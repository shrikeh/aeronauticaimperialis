<?php
namespace Wargame;
use \Wargame\Common\RenderableNamedEntityInterface;

class Phase implements RenderableNamedEntityInterface
{
    protected $_name;

    protected $_actions;

    public function __construct($name)
    {
        $this->_name = $name;
    }

    public function __toString()
    {
        return $this->render();
    }

    public function render()
    {
        return $this->getName();
    }

    /**
     * Return the name of the phase.
     */
    public function getName()
    {
        return $this->_name;
    }

}
