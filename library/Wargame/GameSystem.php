<?php
namespace Wargame;
use \Wargame\GameSystem\GameSystemException;
use \Wargame\Tools\RangeRuler;
use \Wargame\GameSystem\GameSystemAbstract;
class GameSystem extends GameSystemAbstract
{

    protected $_bases = array();

    /**
    * @var \Wargame\RangeRuler
    */
    protected $_rangeRuler;

    /**
    *
    * @var \Wargame\Tools\ShapeBuilder
    */
    protected $_shapeBuilder;


    protected $_preMeasurement = false;


    public function getRangeRuler()
    {
        if (!isset($this->_rangeRuler)) {
            $options = $this->getOption('measurement');
            $this->_setRangeRuler($options);
        }
        return $this->_rangeRuler;
    }

    protected function _setRangeRuler($options)
    {
        $system = $options['system'];
        $units = $options['units'];
        $this->_rangeRuler = new RangeRuler($system, $units);
    }

    public function getBase($baseName)
    {
        if (!array_key_exists($baseName, $this->_bases)) {
            $baseOptions = $this->getOption('bases');
            if (!array_key_exists($baseName, $baseOptions)) {
                throw new GameSystemException("This game does not have a base of type $baseName");
            }
            $shapeBuilder = $this->getShapeBuilder();
            $shape = $shapeBuilder->build($baseOptions[$baseName]);
            $this->_bases[$baseName] = $shape;
        }
        return $this->_bases[$baseName];
    }

    public function getShapeBuilder()
    {
        if (!isset($this->_shapeBuilder)) {
            $options = $this->getOption('shapebuilder');
            $className = $options['class'];
            $this->_shapeBuilder = new $className($this->getRangeRuler());
        }
        return $this->_shapeBuilder;
    }
}
