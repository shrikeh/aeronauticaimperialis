<?php
/**
 * A Game system is a holder for all of the components we need to actually play a game. It contains
 * the rule book, the tools, and all meta information needed for a Game object.
 * @category          Wargame
 * @package           GameSystem
 * @copyright         Copyright (c) 2012 Barney Hanlon (http://www.shrikeh.net)
 * @author            B Hanlon <barney@shrikeh.net>
 * @version           Revision $LastChangedRevision$ by $LastChangedBy$ on $LastChangedDate$
 * @access            public
 */
namespace Wargame\GameSystem;
use \Wargame\Common\RenderableNamedEntityInterface;
use \Wargame\Phase;
use \Wargame\Common\OptionedEntityInterface;
/**
 * A Game system is a holder for all of the components we need to actually play a game. It contains
 * the rule book, the tools, and all meta information needed for a Game object.
 * @category          Wargame
 * @package           GameSystem
 * @copyright         Copyright (c) 2012 Barney Hanlon (http://www.shrikeh.net)
 * @author            B Hanlon <barney@shrikeh.net>
 * @version           Revision $LastChangedRevision$ by $LastChangedBy$ on $LastChangedDate$
 * @access            public
 */
abstract class GameSystemAbstract implements GameSystemInterface, RenderableNamedEntityInterface
{

   /**
    * Associative rray of options
    * @var array
    */
    protected $_options = array();


    /**
     * Constructor.
     * @param Zend_Config|array $config
     */
    public function __construct($config)
    {
        $this->setOptions($config);
    }

    /**
    * (non-PHPdoc)
    * @see Wargame\Common.RenderableInterface::__toString()
    */
    public function __toString()
    {
        return $this->render();
    }

    public function __get($var)
    {
        $key = '_' . $var;
        if (property_exists($this, $key)) {
            return $this->$key;
        }
        if (array_key_exists($var, $this->_options)) {
            return $this->_options[$var];
        }
        throw new GameSystemException("Unknown variable $var");
    }

    public function render()
    {
        return $this->getName();
    }

    public function getName()
    {
        return $this->_name;
    }

    /**
     * If there is a maximum number of turns
     * @var integer|null
     */
    protected $_phases;

    public function getPhases()
    {
        if (isset($this->_phases)) {
            $options = $this->getOption('phases');
            $this->_setPhases($options);
        }
        return $this->_phases;
    }

    public function getAuthor()
    {
        return $this->_author;
    }

    public function hasOption($option)
    {
        return array_key_exists($option, $this->_options);
    }

    public function setOptions($config = array())
    {
        if ($config instanceof \Zend_Config) {
            $config = $config->toArray();
        }
        foreach ($config as $key => $value) {
            $this->setOption($key, $value);
        }
    }

    public function setOption($key, $value)
    {
        $method = '_set' . strtolower($key);
        if (method_exists($this, $method)) {
            return $this->$method($value);
        }
        $this->_options[$key] = $value;
        return;
    }

    public function getOptions()
    {
        return $this->_options;
    }

    public function getOption($optionName)
    {
        if (!$this->hasOption($optionName)) {
            throw new OptionedEntityInterface("You tried to get $optionName, but this has not been set");
        }
        return $this->_options[$optionName];
    }


    protected function _setName($name)
    {
        $this->_name = $name;
    }

    protected function _setAuthor($author)
    {
        $this->_author = $author;
    }

    protected function _setPhases(array $phases = array())
    {

    }

    /**
     * Return the maximum number of turns
     * @return integer|null
     */
    public function getTurnLimit()
    {
        return $this->_turnLimit;
    }

    public function getAllowPreMeasure()
    {
        return $this->_preMeasurement;
    }

    public function getPhase($phaseName)
    {

    }

    public function setPhase(Phase $phase, $position = null)
    {
        $this->_phases[$position] = $phase;
    }


    public function offsetExists($offset)
    {
        return isset($this->_phases[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->getPhase($offset);
    }

    public function offsetSet($offset, $value)
    {
        return $this->setPhase($value, $offset);
    }

    public function offsetUnset($offset)
    {

    }

    public function current()
    {

    }

    public function next()
    {

    }

    public function key()
    {

    }

    public function valid()
    {

    }

    public function rewind()
    {

    }
}
