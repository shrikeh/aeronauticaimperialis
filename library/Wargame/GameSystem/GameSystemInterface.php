<?php
namespace Wargame\GameSystem;
use \Wargame\Common\OptionedEntityInterface;

interface GameSystemInterface extends OptionedEntityInterface, \ArrayAccess, \Iterator
{
    public function getPhases();
}
