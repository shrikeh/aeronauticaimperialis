<?php
namespace Wargame\Geometry;
use \Euclidean\Coordinate\CoordinateInterface;
use \Wargame\Geometry\Arc\Sector;

class Arc
{


    /**
     * The centre of the arc
     * @var \Euclidean\Coordinate\CoordinateInterface
     */
    protected $_coordinate;

    protected $_sectors;

    public function __construct(CoordinateInterface $coordinate, $sectors)
    {
        $this->_coordinate = $coordinate;
        $this->_sectors = new \SplObjectStorage();
        foreach ($sectors as $sector) {
            $this->_addSector($sector);
        }
    }

    public function __invoke($degree)
    {
        return $this->in($degree);
    }

    /**
     * Set the sectors
     * @param Band $band
     * @throws \RangeException
     */
    protected function _addSector(Sector $sector)
    {
        foreach ($this->_sectors as $registeredSector) {
            if ( ($registeredBand->in($band->getStart())) || ($registeredBand->in($band->getAngle())) {
                throw new \RangeException("You tried to set an arc sector that is in an existing sector");
            }
        }
        $this->_sectors->attach($sector);
    }

    public function getCoordinate()
    {
        return $this->_coordinate;
    }

    /**
     *
     * @param float $degree
     * @return Wargame\Geometry\Arc\Sector|boolean
     */
    public function in($degree)
    {
        foreach ($this->_sectors as $sector) {
            if ($sector->in($degree)) {
                return $sector;
            }
        }
        return false;
    }
}
