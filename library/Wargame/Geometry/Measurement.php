<?php
namespace Wargame\Geometry;
use \Wargame\Tools\RangeRuler;
use \Wargame\Common\RenderableInterface;
class Measurement implements RenderableInterface
{

    const INCH_MICRO = 25400; // number of μm in an inch

    const CM_MICRO = 10000; // number of μm in a cm

    protected $_value;

    protected $_suffix;

    protected $_unit;


    public function __construct($value, $unit = RangeRuler::INCHES, $suffix = null)
    {
        $this->_value = $value;
        $this->_unit = $unit;
        $this->_suffix = $suffix;
    }

    public function __toString()
    {
        return $this->render();
    }

    public function setSuffix($suffix = null)
    {
        $this->_suffix = (string) $suffix;
    }


    /**
     * Render out the value
     * @return string
     */
    public function render()
    {
        return (string) $this->_value . $this->_unit . $this->_suffix;
    }

    public function toValue()
    {
        return $this->_value;
    }


    /**
     *
     * @param string $unit
     * @return integer
     */
    public function toUnit($unit)
    {
        $baseValue = $this->toBase(); // now have value in micrometres
        $divisor = $this->_baseUnit($unit);
        return round($baseValue / $divisor, 2);
    }


    /**
     *
     * @param string $unit
     * @return integer
     */
    protected function _baseUnit($unit)
    {
        switch ($unit) {
            case RangeRuler::INCHES :
                return self::INCH_MICRO;
                break;
            case RangeRuler::CENTIMETRES :
                return self::CM_MICRO;
                break;
            default :
                return 1;
        }
    }

    /**
     * Return the value in micrometres;
     * @return integer
     */
    public function toBase()
    {
        return round($this->_value * $this->_baseUnit($this->_unit), 0);
    }

}
