<?php
namespace Wargame\Geometry\Arc;

class Sector
{

    /**
    * The start of this arc, i.e. "from 90 degrees"
    * @var integer
    */
    protected $_start;

    /*
     * The angle as described in degrees.
     * @var integer
     */
    protected $_angle;

    public function __construct($start, $angle)
    {
        $this->_start = $start;
        $this->_angle = $angle;
    }

    public function __invoke($angle)
    {
        return $this->in($angle);
    }
    /*
     * Whether or not the designated angle is within this sector
     * @return boolean
     */
    public function in($angle)
    {
        $end = $this->_start + $this->_angle;
        return (($angle >= $this->_start) && ($angle < $end));
    }

    /**
     * Return the start angle
     * @return number
     */
    public function getStart()
    {
        return $this->_start;
    }

    /*
     * Return the angle
    * @return integer
    */
    public function getAngle()
    {
        return $this->_angle;
    }
}
