<?php
namespace Wargame\Geometry;
use \Wargame\Geometry\Measurement;
use \Wargame\Geometry\Range\Band;
class Range
{
    protected $_bands;

    public function __construct($bands)
    {
        $this->_bands = new \ArrayIterator;
        foreach ($bands as $band) {
            $this->_addBand($band);
        }
    }

    public function __invoke(Measurement $measurement)
    {
        return $this->in($measurement);
    }

    /**
     * Set the bands
     * @param Band $band
     * @throws \RangeException
     */
    protected function _addBand(Band $band)
    {
        foreach ($this->_bands as $registeredBand) {
            if ( ($registeredBand->in($band->getStart())) || ($registeredBand->in($band->getEnd())) ) {
                throw new \RangeException("You tried to set a range band that is in an existing range");
            }
        }
        $this->_bands->append($band);
    }

    /**
     * See whether something is within another range
     * @param integer $distance
     * @return \Wargame\Range\Band|boolean
     */
    public function in(Measurement $distance)
    {
        foreach ($this->_bands as $band) {
            if ($band->in($distance)) {
                return $band;
            }
        }
        return false;
    }
}
