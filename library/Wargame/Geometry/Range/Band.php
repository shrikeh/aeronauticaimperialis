<?php
namespace Wargame\Geometry\Range;
use \Wargame\Geometry\Measurement;
class Band
{
    /*
    * The name of this band, i.e. "short range"
    * @var string | null
    */
    protected $_name;

    /**
    * The start of this band
    * @var \Wargame\Measurement
    */
    protected $_start;

    /**
     * The start of this band
     * @var \Wargame\Measurement
     */
    protected $_end;

    /**
     * Constructor
     * @param Measurement $bandStart
     * @param Measurement $bandEnd
     * @param string|null $name
     * @throws \LengthException if the start is larger then the end
     */
    public function __construct(Measurement $bandStart, Measurement $bandEnd, $name = null)
    {
        if ($bandStart->toBase() > $bandEnd->toBase()) {
            throw new \LengthException('The start of the band must be less then the end value');
        }
        $this->_name = $name;
        $this->_start = $bandStart;
        $this->_end = $bandEnd;
    }

    public function __invoke(Measurement $measurement)
    {
        return $this->in($measurement);
    }

    /**
     * Return the name of this band
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }
    /**
     * See whether a measurement falls within a band
     * @param Measurement $range
     * @return boolean
     */
    public function in(Measurement $range)
    {
        if ($range->toBase() < $this->getStart()->toBase()) {
            return false;
        }
        if ($this->isInfinite()) {
            return true;
        }
        if ($range->toBase() >= $this->getEnd()->toBase()) {
            return false;
        }
        return true;
    }

    /**
     * Whether or not this band extends to infinity
     * @return boolean
     */
    public function isInfinite()
    {
        return is_infinite($this->_end->toBase());
    }

    /**
     * Return the start of this band in micrometers
     * @return \Wargame\Measurement
     */
    public function getStart()
    {
        return $this->_start;
    }
    /**
     * Return the end of this band in micrometers
     * @return \Wargame\Measurement
     */
    public function getEnd()
    {
        return $this->_end;
    }
}
