<?php
use \Zend_Controller_Action as ActionController;
class User_LoginController extends ActionController
{
    public function indexAction()
    {
        $auth = $this->_helper->auth();
        if (!$auth->hasIdentity()) {
            $this->_forward('login');
        }

        $this->_helper->view->setResponseSegment('auth');
        $this->view->user = $auth->getIdentity();
    }

    public function loginAction()
    {

    }

    public function logoutAction()
    {

    }

}
