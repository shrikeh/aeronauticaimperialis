<?php
use \Zend_Application_Module_Bootstrap as ModuleBootstrap;
class Main_Bootstrap extends ModuleBootstrap
{
    protected function _initRouter()
    {
        $this->bootstrap('frontController');
        $front = $this->getResource('frontController');
        // ZF-6545: ensure front controller resource is loaded
        $configManagerResource = $this->getApplication()->getPluginResource('configmanager');
        $router = $front->getRouter();

        $routes =  $configManagerResource->getConfig(
                __DIR__ . '/configs/routes.ini',
                strtolower($this->getModuleName())
        );
        if ($routes instanceof Anobii_Cache_Frontend_Config) {
            $routes = $routes->getCachedEntity();
        }

        $router->addConfig($routes);
        return $router;
    }
}
