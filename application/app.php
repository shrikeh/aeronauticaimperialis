<?php
// Define path to application directory
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', __DIR__ . '/application');

defined('RESOURCE_PATH')
|| define('RESOURCE_PATH', __DIR__ . '/public');

// Define application environment
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
$includePath = explode(PATH_SEPARATOR,get_include_path());
$includePath[] = __DIR__ . '/library';
$includePath[] = __DIR__ . '/library/vendor';

foreach ($includePath as $path) {
    $truePath[] = realpath($path);
}
set_include_path(implode(PATH_SEPARATOR, array_unique($truePath)));

/** Zend_Application */
require_once 'Zend/Application.php';
// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
return $application;