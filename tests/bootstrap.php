<?php
// PHP 5.3 Compat
date_default_timezone_set('Europe/London');

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', __DIR__ . '/../application');

    // Define path to application tests directory
defined('APPLICATION_TESTS_PATH')
    || define('APPLICATION_TESTS_PATH' ,__DIR__);

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/../library/vendor'),
    get_include_path()
)));


require_once 'Zend/Loader/Autoloader.php';

$autoloader = \Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace(array(
    'AeronauticaImperialis',
    'Wargame',
    'Euclidean'
));

if (extension_loaded('xdebug')) {
    ini_set('xdebug.show_exception_trace', 0);
}

