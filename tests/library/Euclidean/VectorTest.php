<?php
namespace Tests\Euclidean;
use \PHPUnit_Framework_TestCase as TestCase;
use \Euclidean\Vector;
use \Euclidean\Coordinate\Cartesian;
class VectorTest extends TestCase
{

    public function testSpeedAndMovement()
    {
        // landed aircraft
        $cartesianOne = new Cartesian(24, 24, 1);
        $vector = new Vector($cartesianOne, 90, 0);
        $this->assertSame(0, $vector->getSpeed());
        $vector->setSpeed(2);
        $this->assertSame(2, $vector->getSpeed());

        $vector->move();

        $cartesianTwo = new Cartesian(24, 24, 1);

        $this->assertEquals($cartesianTwo->getDisTance($vector->getCoordinate()), 2);

        $this->assertEquals($vector->getCoordinate()->render(), '(26,24,1)');
    }


}
