<?php
namespace Tests\Euclidean\Coordinate;
use \PHPUnit_Framework_TestCase as TestCase;
use \Euclidean\Coordinate\Cartesian;

class CartesianTest extends TestCase
{

    public function providerCartesian()
    {
        $coordinateOne = new Cartesian(24, 24);

        return array(
                array(
                        $coordinateOne,
                        new Cartesian(24, 24, 1),
                        0, 1, 0, 0
                ),
                array(
                        $coordinateOne,
                        new Cartesian(48, 48, 1),
                        33.94, 1, 45, 225,
                ),
        );
    }

    /**
     * @dataProvider providerCartesian
     */
    public function testCorrectFromOtherCoordinate(
            $cartesianOne,
            $cartesianTwo,
            $distance,
            $height,
            $angleTo,
            $angleFrom
    )
    {
        $this->assertEquals($distance,round($cartesianOne->getDistance($cartesianTwo), 2));
        $this->assertEquals($angleTo, round($cartesianOne->angleTo($cartesianTwo), 0));
        $this->assertEquals($angleFrom, round($cartesianOne->angleFrom($cartesianTwo), 0));
    }
}
