<?php
namespace Tests\Euclidean;
use Euclidean\Coordinate\Spherical;

use \PHPUnit_Framework_TestCase as TestCase;
use \Euclidean\Vector;
use \Euclidean\Path;
use \Euclidean\Coordinate\Cartesian;

class PathTest extends TestCase
{

    public function testAlterByVector()
    {
        $cartesian = new Cartesian(24, 24, 1);
        $vector = new Vector($cartesian, 0, 0);

        $path = new Path();

        for ($i=0; $i<360; $i+=45) {
            $spherical = new Spherical(
                6,
                $i,
                0
            );
            $spherical->setOrigin($vector->getCoordinate());
            $path->addCoordinate($spherical);
        }

        $vector->setHeading(45);
        $vector->setSpeed(6);
        $vector->move();
        $return = $path->by($vector->getHeading());

        //$this->assertNotSame($values, $one);
    }
}
