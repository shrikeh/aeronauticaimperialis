<?php
namespace Tests\Euclidean\Coordinate;
use \PHPUnit_Framework_TestCase as TestCase;
use \Euclidean\Coordinate\Cartesian;
use \Euclidean\Coordinate\Spherical;

class SphericalTest extends TestCase
{
    public function testSimpleSpherical()
    {
        $originOne = new Cartesian(0, 0, 0);

        $spherical = new Spherical(6, 0, 0);
        $spherical->setOrigin($originOne);

        $this->assertEquals(6, $spherical->getY());
        $this->assertEquals(0, $spherical->getX());

        $spherical = new Spherical(6, 90, 0);
        $spherical->setOrigin($originOne);
        $this->assertEquals(0, $spherical->getY());
        $this->assertEquals(6, $spherical->getX());

        $originTwo = new Cartesian(12, 24, 0);
        $spherical = new Spherical(6, 180, 0);
        $spherical->setOrigin($originTwo);
        $this->assertEquals(18, $spherical->getY());
        $this->assertEquals(12, $spherical->getX());
    }
}
