<?php
namespace Tests\Wargame\Tools;
use \PHPUnit_Framework_TestCase as TestCase;
use \Wargame\Tools\RangeRuler;

class RangeRulerTest extends TestCase
{
    public function providerDistances()
    {
        return array(
            array(
                RangeRuler::IMPERIAL,
                RangeRuler::INCHES,
                1,
                25400
            ),
            array(
                RangeRuler::METRIC,
                RangeRuler::CENTIMETRES,
                1,
                10000
            ),
            array(
                RangeRuler::METRIC,
                RangeRuler::CENTIMETRES,
                32,
                320000
            ),
            array(
                RangeRuler::METRIC,
                RangeRuler::CENTIMETRES,
                41.5,
                415000
            ),
        );
    }

    /**
    * @dataProvider providerDistances
    * @param string $system
    * @param string $unit
    * @param float $distance
    * @param integer $baseDistance
    */
    public function testDistances($system, $unit, $distance, $baseDistance)
    {
        $ruler = new RangeRuler($system, $unit);
        $this->assertEquals($system, $ruler->getSystem());
        $this->assertEquals($unit, $ruler->getUnit());
        $this->assertEquals($baseDistance, $ruler->toBase($distance));
        $this->assertEquals($distance, $ruler->fromBase($baseDistance));
        $measurement = $ruler->getMeasurement($distance);
        $this->assertInstanceOf('\Wargame\Geometry\Measurement', $measurement);
        $this->assertEquals($distance, $measurement->toValue());
    }
}
