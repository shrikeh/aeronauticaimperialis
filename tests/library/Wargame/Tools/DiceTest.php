<?php
namespace Tests\Wargame;
use \PHPUnit_Framework_TestCase as TestCase;
use \Wargame\Tools\Dice;
use \Wargame\Tools\Dice\Generator\MersenneTwister;
class DiceTest extends TestCase
{

    public function testRandomEnough()
    {
        $total = 10000;
        $splFixed = new \SplFixedArray($total);
        $generator = new MersenneTwister();
        for ($i = 0; $i!=$total; $i++) {
            $dice = new Dice(6, $generator);
            $splFixed->offsetSet($i, $dice->getValue());
        }
        $spread = array_count_values($splFixed->toArray());
        $this->assertEquals(6, count($spread));
        $sum = array_sum($spread);
        $this->assertEquals($total, $sum);
    }
    /**
    * @expectedException \Wargame\Tools\Dice\DiceException
    * @expectedExceptionMessage A dice can only be re-rolled once!
    **/
    public function testReRoll()
    {
        $xdebugSetting = ini_get('xdebug.show_exception_trace');
        ini_set('xdebug.show_exception_trace', 0);
        $generator = new MersenneTwister();
        $dice = new Dice(6, $generator);
        $dice->setValue(3);
        $this->assertFalse($dice->hasRerolled());
        $this->assertEquals(3, $dice->getValue());
        $this->assertFalse($dice->hasRerolled());
        $dice->reRoll(4);
        $this->assertNotEquals(3, $dice->getValue());
        $this->assertEquals(4, $dice->getValue());
        $this->assertTrue($dice->hasRerolled());
        $dice->reRoll();
        ini_set('xdebug.show_exception_trace', $xdebugSetting);
    }

    public function testSetGenerator()
    {
        $generator = $this->getMockBuilder('\Wargame\Tools\Dice\Generator\GeneratorInterface')
            ->getMock();
        $generator->expects($this->at(0))->method('generate')->will($this->returnValue(2));
        $generator->expects($this->at(1))->method('generate')->will($this->returnValue(4));

        $dice = new Dice(6, $generator);
        $this->assertEquals(2, $dice->getValue());
        $dice->setGenerator($generator);
        $dice->reRoll();
        $this->assertEquals(4, $dice->getValue());
    }

    /**
     * @param unknown_type $generator
     * @expectedException \OutOfBoundsException
     **/
    public function testCannotPassBadValues()
    {
        $dice = new Dice();
        $dice->setValue(7);
    }

    public function testDiceValues()
    {
        $dice = new Dice();
        $dice->setValue(4);
        $this->assertEquals(4, (string) $dice);
        $this->assertTrue($dice->equals(4));
        $this->assertTrue($dice->greaterThanOrEqualTo(4));
        $this->assertFalse($dice->greaterThanOrEqualTo(5));
        $this->assertTrue($dice->greaterThan(3));
        $this->assertFalse($dice->greaterThan(4));
        $this->assertTrue($dice->equalToOrLessThan(4));
        $this->assertFalse($dice->lessThan(4));
        $this->assertTrue($dice->lessThan(5));

        for ($i=1; $i!=6; $i++) {
            $this->assertEquals(
                $dice->greaterThanOrEqualTo($i),
                $dice->equalToOrGreaterThan($i)
            );
            $this->assertEquals(
                $dice->lessThanOrEqualTo($i),
                $dice->equalToOrLessThan($i)
            );
        }
    }
}
