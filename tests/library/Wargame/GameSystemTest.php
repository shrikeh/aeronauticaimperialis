<?php
use Wargame\Geometry\Measurement;

namespace Tests\Wargame;
use \PHPUnit_Framework_TestCase as TestCase;
use \Wargame\GameSystem;
use \Wargame\Geometry\Measurement;
use \Wargame\Tools\ShapeBuilder;
use \Zend_Config_Ini as Config;
class GameSystemTest extends TestCase
{

    public function providerAeronauticaConfig()
    {
        return array(array($this->_getConfig('aeronautica-imperialis')));
    }

    public function providerDystopianWarsConfig()
    {
        return array(array($this->_getConfig('dystopian-wars')));
    }

    public function provider40kConfig()
    {
        return array(array($this->_getConfig('wh40k')));
    }

    protected function _getConfig($configName)
    {
        $filename = APPLICATION_PATH . "/configs/gamesystems/$configName.ini";
        $config = new Config($filename, $configName);
        return $config;
    }

    /**
     * @dataProvider providerAeronauticaConfig
     */
    public function testSetOptions($config)
    {
        $config = $config->gamesystem->ai;
        $gameSystem = new GameSystem($config);

        $this->assertEquals($config->name, $gameSystem->getName());
        $this->assertEquals($config->author, $gameSystem->getAuthor());
        $this->assertInstanceOf('\Wargame\Tools\RangeRuler', $gameSystem->getRangeRuler());
    }

    public function testGetBaseSizes()
    {
        $config = $this->_getConfig('dystopian-wars');
        $gameSystem = new GameSystem($config->gamesystem->dw);
        $this->assertInstanceOf('\Wargame\Tools\ShapeBuilder', $gameSystem->getShapeBuilder());
        $baseOptions = $config->gamesystem->dw->bases->toArray();
        foreach ($baseOptions as $key => $base) {
            $shape = $gameSystem->getBase($key);
            $measurementWidth = null;
            $measurementLength = null;
            if (array_key_exists('width', $base)) {
                $width = $base['width'];
                $unit = key($width);
                $value = current($width);
                $measurementWidth = new Measurement($value, $unit);
                $this->assertEquals($measurementWidth->toBase(), $shape->getWidth());
            }

            if (array_key_exists('length', $base)) {
                $length = $base['length'];
                $unit = key($length);
                $value = current($length);
                $measurementLength = new Measurement($value, $unit);
                $this->assertEquals($measurementLength->toBase(), $shape->getLength());
            }

            if ( ($measurementWidth) && (!$measurementLength) ) {
                $this->assertEquals($measurementWidth->toBase(), $shape->getLength());
            }
        }
    }
}
