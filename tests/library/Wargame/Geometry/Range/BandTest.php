<?php
namespace Tests\Wargame\Geometry\Range;
use \PHPUnit_Framework_TestCase as TestCase;
use \Wargame\Geometry\Range\Band;
use \Wargame\Tools\RangeRuler;
use \Wargame\Geometry\Measurement;

class BandTest extends TestCase
{
    protected $_rangeRuler;

    public function setUp()
    {
        parent::setUp();
        $this->_rangeRuler = new RangeRuler(RangeRuler::IMPERIAL, RangeRuler::INCHES);
    }

    /**
     * @expectedException LengthException
     */
    public function testException()
    {
        $start = $this->_rangeRuler->getMeasurement(6);
        $end = $this->_rangeRuler->getMeasurement(5.99);
        $band = new Band($start, $end, 'medium');
    }

    public function testInRange()
    {
        $start = $this->_rangeRuler->getMeasurement(6);
        $end = $this->_rangeRuler->getMeasurement(12);
        $inRange = $this->_rangeRuler->getMeasurement(7.5);

        $band = new Band($start, $end, 'medium');
        $this->assertInstanceOf('\Wargame\Geometry\Measurement', $band->getStart());
        $this->assertTrue($band->in($inRange));

        $outOfRange = $this->_rangeRuler->getMeasurement(12.01);
        $this->assertFalse($band->in($outOfRange));
        $this->assertFalse($band->isInfinite());

        $infiniteBand = new Band(
            $end,
            $this->_rangeRuler->getMeasurement(INF),
            'infinite'
        );
        $this->assertTrue($infiniteBand->isInfinite());
        $this->assertTrue($infiniteBand->in($outOfRange));
        $this->assertFalse($infiniteBand->in($start));
        // test __invoke()
        $this->assertSame($infiniteBand->in($outOfRange), $infiniteBand($outOfRange));
    }
}
