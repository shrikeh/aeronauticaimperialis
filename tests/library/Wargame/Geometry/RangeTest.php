<?php
namespace Tests\Wargame\Geometry;
use \PHPUnit_Framework_TestCase as TestCase;
use \Wargame\Geometry\Range;
use \Wargame\Geometry\Range\Band;
use \Wargame\Tools\RangeRuler;
use \Wargame\Geometry\Measurement;

class BandTest extends TestCase
{
    protected $_rangeRuler;

    public function setUp()
    {
        parent::setUp();
        $this->_rangeRuler = new RangeRuler(RangeRuler::IMPERIAL, RangeRuler::INCHES);
    }

    public function testInRange()
    {
        $rangeRuler = $this->_rangeRuler;
        $short = new Band(
            $rangeRuler->getMeasurement(0),
            $rangeRuler->getMeasurement(6),
            'short'
        );
        $medium = new Band(
            $rangeRuler->getMeasurement(6),
            $rangeRuler->getMeasurement(12),
            'medium'
            );
        $long = new Band(
            $rangeRuler->getMeasurement(12),
            $rangeRuler->getMeasurement(18),
            'long'
        );
        $bands = array(
            $short,
            $medium,
            $long,
        );

        $range = new Range($bands);
        $inRange = $rangeRuler(5);
        $result = $range->in($inRange);
        $this->assertInstanceOf('\Wargame\Geometry\Range\Band', $result);
        $this->assertSame($short, $result);

        $outOfRange = $rangeRuler(18.01);
        $result = $range->in($outOfRange);
        $this->assertFalse($result);

    }

    /**
     * @expectedException \RangeException
     */
    public function testRangeUniqueException()
    {
        $bands = array(
            new Band(
                $this->_rangeRuler->getMeasurement(0),
                $this->_rangeRuler->getMeasurement(6)
            ),
            new Band(
                $this->_rangeRuler->getMeasurement(5),
                $this->_rangeRuler->getMeasurement(9)
            ),
        );
        $range = new Range($bands);
    }
}
