<?php
namespace Tests\AeronauticaImperialis;
use \PHPUnit_Framework_TestCase as TestCase;
use \AeronauticaImperialis\Race;

class RaceTest extends TestCase
{
	public function providerRaces()
	{
		return array(
			array(
				new Race('Orks', 5), 'Orks', 5
			),
			array(
				new Race('Space Marines', 3), 'Space Marines', 3
			),	
			array(
				new Race('Eldar', 3), 'Eldar', 3
				),					
		);
	}
	
	/**
	 * @dataProvider providerRaces
	 **/
	public function testRaces($race, $name, $skill)
	{
		$this->assertEquals($race->getName(), $name);
		$this->assertEquals($race->getBaseSkill(), $skill);		
	}
}