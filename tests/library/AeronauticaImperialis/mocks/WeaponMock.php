<?php
namespace Tests\AeronauticaImperialis\Mocks;
use \AeronauticaImperialis\Weapon;
class WeaponMock extends Weapon
{
	public static function resetStaticMaps()
	{
		self::$_rangeNameMap = null;
		self::$_rangeDistanceMap = null;
	}
	
	public static function getStaticRangeNameMap()
	{
		return self::$_rangeNameMap;
	}
	
	public static function getStaticRangeDistanceMap()
	{
		return self::$_rangeDistanceMap;
	}
}