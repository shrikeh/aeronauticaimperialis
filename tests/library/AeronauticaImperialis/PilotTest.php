<?php
namespace Tests\AeronauticaImperialis;
use \PHPUnit_Framework_TestCase as TestCase;
use \AeronauticaImperialis\Pilot;
use \AeronauticaImperialis\Race;
use \AeronauticaImperialis\Dice;
class PilotTest extends TestCase
{
    public function testAceCreation()
    {
        $race = new Race('Orks', 5);

        $pilot = new Pilot($race, 'Da Black Barun', Pilot::ACE);
		$this->assertEquals($race, $pilot->getRace());
        $this->assertEquals($pilot->getSkill(), 4);

        $this->assertTrue($pilot->isAce());
    }

    public function testBecomingAce()
    {
        $race = new Race('Orks', 5);

        $pilot = new Pilot($race, 'Da Black Barun');
		$dice = new Dice(4);
		// start adding on some kills
		for ($i=0; $i<=4; $i++) {
	        $this->assertFalse($pilot->isAce());
	        $this->assertEquals($pilot->getSkill(), 5);
	        $this->assertFalse($pilot->isDoubleAce());
			$this->assertEquals($i, $pilot->getKills());
			$this->assertFalse($pilot->skillTest($dice));
			$pilot->addKill();	
		}
		
		for ($i=1; $i<=4; $i++) {
			$pilot->addKill();
	        $this->assertTrue($pilot->isAce());
			$this->assertFalse($pilot->isDoubleAce());
	        $this->assertEquals($pilot->getSkill(), 4);
			$this->assertTrue($pilot->skillTest($dice));
		}
		for ($i=1; $i<=5; $i++) {
			$pilot->addKill();
			$this->assertTrue($pilot->isAce());
			$this->assertTrue($pilot->isDoubleAce());
			$this->assertEquals($pilot->getSkill(), 3);
		}

    }
}
