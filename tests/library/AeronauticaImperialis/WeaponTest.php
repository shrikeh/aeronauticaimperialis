<?php
namespace Tests\AeronauticaImperialis;
use \PHPUnit_Framework_TestCase as TestCase;
use \Tests\AeronauticaImperialis\Mocks\WeaponMock as Weapon;
use \AeronauticaImperialis\Dice;
require_once __DIR__ . '/mocks/WeaponMock.php';
class WeaponTest extends TestCase
{
	protected function _getSustainedFireWeapon()
	{
		// burst cannon
		$weapon = new Weapon(
			6,
			array(1, 1, 0), 
			Weapon::UNLIMITED_AMMO
		);	
		return $weapon;		
	}
	
	protected function _getLimitedExtraDamageWeapon()
	{
		// lascannon
		$weapon = new Weapon(
			2, 
			array(0, 1, 1), 
			4, 
			5
		);	
		return $weapon;
	}
	
	public function providerLimitedExtraDamageWeapon()
	{
		$return = array(
			array(
				$this->_getLimitedExtraDamageWeapon(),
				Weapon::RANGE_DISTANCE_SHORT,
				false,
				0,
				3
			),
			array(
				$this->_getLimitedExtraDamageWeapon(),
				Weapon::RANGE_DISTANCE_MEDIUM,
				false,
				1,
				3
			),				
			array(
				$this->_getLimitedExtraDamageWeapon(),
				Weapon::RANGE_DISTANCE_LONG,
				false,
				1,
				3
			),	
			array(
				$this->_getLimitedExtraDamageWeapon(),
				Weapon::RANGE_DISTANCE_LONG,
				true,
				2,
				2
			),			
		);
		return $return;
	}
	
	public function testSustainedFireWeapon()
	{

		$weapon = $this->_getSustainedFireWeapon();
		for ($i=0; $i<1000; $i++) {
			$this->assertEquals(1, $weapon->fire(Weapon::RANGE_DISTANCE_SHORT));
			$weapon->resetHasFired();
		}
		$xdebugSetting = ini_get('xdebug.show_exception_trace');
		ini_set('xdebug.show_exception_trace', 0);
		$this->setExpectedException('AeronauticaImperialis\Weapon\WeaponException');
		$weapon->fire(Weapon::RANGE_DISTANCE_SHORT, true);
		ini_set('xdebug.show_exception_trace', $xdebugSetting);
	}
	
	
	public function providerDamage()
	{
		return array(
			array(
				$this->_getLimitedExtraDamageWeapon(),
				new Dice(1),
				0
			),
			array(
				$this->_getLimitedExtraDamageWeapon(),
				new Dice(2),
				1
			),
			array(
				$this->_getLimitedExtraDamageWeapon(),
				new Dice(5),
				2
			),
			array(
				$this->_getLimitedExtraDamageWeapon(),
				new Dice(6),
				2
			),
		);
	}
	
	/**
	 * @dataProvider providerDamage
	 **/	
	public function testDoesDamage($weapon, $dice, $expected)
	{
		$this->assertEquals(
			$weapon->doesDamage($dice), 
			$expected
		);
	}
	
	/**
	 * @expectedException \AeronauticaImperialis\Weapon\WeaponException
	 * @expectedExceptionMessage Weapon has already fired this turn
	 **/	
	public function testCannotFireWeaponTwice()
	{
		$xdebugSetting = ini_get('xdebug.show_exception_trace');
		ini_set('xdebug.show_exception_trace', 0);
		$weapon = $this->_getLimitedExtraDamageWeapon();
		$weapon->fire(Weapon::RANGE_DISTANCE_LONG);
		$weapon->fire(Weapon::RANGE_DISTANCE_LONG);
		ini_set('xdebug.show_exception_trace', $xdebugSetting);
	}
	
	/**
	 * @dataProvider providerLimitedExtraDamageWeapon
	 **/
	public function testFireLimitedAmmoWeaponTillItRunsOutOfBullets(
		$weapon, 
		$range, 
		$sustained, 
		$expectedDice, 
		$ammoRemaining
	)
	{
		$this->assertTrue($weapon->hasAmmo());
		$this->assertEquals($weapon->fire($range, $sustained), $expectedDice);
		$this->assertEquals($weapon->getCurrentAmmo(), $ammoRemaining);
		$weapon->resetHasFired();
		if ($ammoRemaining) {
			$this->assertTrue($weapon->hasAmmo());
			for ($i = $ammoRemaining; $i > 0; $i--) {
				$this->assertEquals($weapon->getCurrentAmmo(), $i);	
				$weapon->fire($range);
				$weapon->resetHasFired();			
			}
			$this->assertFalse($weapon->hasAmmo());
		}
	}
	
	public function providerRanges()
	{
		return array(
			array(
				$this->_getLimitedExtraDamageWeapon(),
				5,
				0
			),
			array(
				$this->_getLimitedExtraDamageWeapon(),
				6,
				0
				),
			array(
				$this->_getLimitedExtraDamageWeapon(),
				7,
				1
			),
			array(
				$this->_getLimitedExtraDamageWeapon(),
				13,
				1
			),
		array(
			$this->_getLimitedExtraDamageWeapon(),
			17.9,
			1
		),
	array(
		$this->_getLimitedExtraDamageWeapon(),
		18.01,
		null
	),		
			array(
				$this->_getLimitedExtraDamageWeapon(),
				null,
				null
			),
			array(
				$this->_getSustainedFireWeapon(),
				5,
				1
			),
			array(
				$this->_getSustainedFireWeapon(),
				19,
				null
			),			
		);
	}
	
	public function testDistanceMap()
	{
		Weapon::resetStaticMaps();
		
		$this->assertEquals(null, Weapon::getStaticRangeDistanceMap());
		$this->assertEquals(null, Weapon::getStaticRangeNameMap());
		
		$distanceMap = Weapon::getRangeDistanceMap()->toArray();
		$this->assertContains(6, $distanceMap);
		$this->assertContains(12, $distanceMap);
		$this->assertContains(18, $distanceMap);
		
		$this->assertNotEquals(null, Weapon::getStaticRangeDistanceMap());
		$this->assertNotEquals(null, Weapon::getStaticRangeNameMap());
				
	}
	
	public function testRangeMap()
	{
		$nameMap = Weapon::getRangeNameMap()->toArray();
		$this->assertContains('short', $nameMap);
		$this->assertContains('medium', $nameMap);
		$this->assertContains('long', $nameMap);
	}

	/**
	 * @dataProvider providerRanges
	 **/	
	public function testRanges($weapon, $range, $dice)
	{
		$this->assertEquals($dice, $weapon->getDiceAtRange($range));
	}
	
	public function testGetters()
	{
		$weapon = $this->_getLimitedExtraDamageWeapon();
		$this->assertEquals(2, $weapon->getDamage());
		$this->assertEquals(5, $weapon->getExtraDamage());
	}
	
	/**
	 * @expectedException \AeronauticaImperialis\Weapon\OutOfAmmoException
	 **/
	public function testCannotShootWeaponOutOfAmmo()
	{
		$xdebugSetting = ini_get('xdebug.show_exception_trace');
		ini_set('xdebug.show_exception_trace', 0);
		$weapon = new Weapon(
			2, 
			array(8, 4, 0), 
			0, 
			5
		);	
		$weapon->fire(Weapon::RANGE_DISTANCE_SHORT);
		ini_set('xdebug.show_exception_trace', $xdebugSetting);
	}
}